var data;
var app = new Vue({
  el: '#app',
  data: {
    members: {}
  }

});

function dataTable(data) {
  //////Get all filters states ///////
  let inputParty = document.querySelectorAll('input[name=party]:checked');
  let inputState = document.querySelector('select[id=state_select').value;
  /////get checkbox node and pass values to array//////
  let filterParty = [];
  let result = [];

  for (let i = 0; i < inputParty.length; i++) {
    filterParty[i] = inputParty[i].value
  }

  ///////iterate in the data and get the values ​​of the fields///// 
  for (let i = 0; i < data.results[0].members.length; i++) {
    let party = "" + data.results[0].members[i].party;
    let state = data.results[0].members[i].state;
    ///////apply the filters///////
    if ((party == filterParty[0] || party == filterParty[1] || party == filterParty[2]) && (inputState == state || inputState == "All")) {

      result.push(data.results[0].members[i])
    }
  }
  console.log(result);
  app.members=result;

}////creates table with data

function state_options(data) {
  let html_State_options = "";
  let stateArray = [];

  ///////get the values ​​of the states and save them in an array////
  for (let i = 0; i < data.results[0].members.length; i++) {
    let state = data.results[0].members[i].state;
    //////to obtain a single copy of each state////
    if (!stateArray.includes(state)) {
      stateArray.push(state)
    }

  }

  //////sort the array alphabetically/////
  stateArray.sort();

  /////////create the option tag for each state////////
  for (let i = 0; i < stateArray.length; i++) {
    html_State_options += "<option>" + stateArray[i] + "</option>"
  }
  $("#state_select").append(html_State_options);
}//// fill the states dropdown menu

/* function get_new_data(api_url) {
  fetch(api_url, {
    headers: {
      'X-API-Key': 'Lp8rTF4WdyaE0rlwyTUXV1EkIJhOtQxsOENKTzpG'
    }
  })
    .then(result => {
      return result.json();
    })
    .then(json => {
      data = json;
      console.log(json);
      myCallback(json);
    })
    .catch(error => alert(error));
} */////request the data and callback function if it is successful///

function myCallback(result) {
  state_options(result);
  dataTable(result)
  data = result;
}////create the table fill the menu and set de data variable////

function change_congress(number, chamber) {
  let api_url = 'https://api.propublica.org/congress/v1/' + number + '/' + chamber + '/members.json';
  get_new_data(api_url);
  $('#congress_title').html("Congress " + number)
}/////update the table when the congress is modified

$(document).ready(function () {

  var congress = 113;//default
  var chamber = selected_chamber;//defined in html page
  var api_url = 'https://api.propublica.org/congress/v1/' + congress + '/' + chamber + '/members.json';//url request

  ////////load default table////////
  get_new_data(api_url);

  //////Event Listeners///////
  $('input[name=party]').change(function () {
    dataTable(data);
  });;

  $('select[id=state_select]').change(function () {
    dataTable(data);
  });;

  $("#change_congress").click(function () {
    let number = document.querySelector('input[id=congressN').value;
    change_congress(number, chamber);
  })

})

