function get_new_data(api_url, cacheKey=api_url) {
  console.log(api_url);
  let cached= sessionStorage.getItem(cacheKey);
  if (cached!=null){
    console.log("cache result");
    return Promise.resolve(new Response(new Blob([cached])))
    .then(response=> response.json())
    .then(json=>{
      data=json;
      console.log(json);
      myCallback(json);
    })
    
  }
  return fetch(api_url, {
    headers: {
      'X-API-Key': 'Lp8rTF4WdyaE0rlwyTUXV1EkIJhOtQxsOENKTzpG'
    }
  })
    .then(result => {
      if(result.status===200){
        result.clone().text().then(content => {
          sessionStorage.setItem(cacheKey, content);
          console.log("almacenando en cache")
        })
      }
      return result.json();
    })
    .then(json=>{
      data=json;
      console.log(json);
      myCallback(json);
    })
    .catch(error => alert(error));
}