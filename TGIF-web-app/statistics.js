var statistics = {
  nro_democrats: 0,
  nro_republicans: 0,
  nro_independents: 0,
  totalMembers: 0,
  totalAvgVotes: 0,
  democrats_loyalty: 0,
  republicans_loyalty: 0,
  independents_loyalty: 0,
  leastLoyal: [],
  mostLoyal: [],
  leastEngaged: [],
  mostEngaged: []
}

var app = new Vue({
  el: '#app',
  data:{
    statistics:this.statistics
  },
  methods: {
    nro_party_votes: function(total_votes, votes_with_party_pct){
      return Math.round(total_votes * votes_with_party_pct/100);
    }
  }
})

function getPartyMembers(data) {
  let partyList = {
    "democrats": [],
    "republicans": [],
    "independents": [],
  }

  for (let i = 0; i < data.results[0].members.length; i++) {
    if (data.results[0].members[i].party == "R") {
      partyList.republicans.push(data.results[0].members[i]);
    } else if (data.results[0].members[i].party == "D") {
      partyList.democrats.push(data.results[0].members[i]);
    } else {
      partyList.independents.push(data.results[0].members[i]);
    }
  }
  return partyList;
}

function updateStatistics() {
  let party_with_members=0;
  if (getAverageVotesWhitParty(democrats_list)){party_with_members++;}
  if (getAverageVotesWhitParty(republicans_list)){party_with_members++;}
  if (getAverageVotesWhitParty(independent_list)){party_with_members++;}
  statistics.nro_republicans = republicans_list.length;
  statistics.nro_democrats = democrats_list.length;
  statistics.nro_independents = independent_list.length;
  statistics.totalMembers = republicans_list.length + democrats_list.length + independent_list.length;
  statistics.democrats_loyalty = getAverageVotesWhitParty(democrats_list);
  statistics.republicans_loyalty = getAverageVotesWhitParty(republicans_list);
  statistics.independents_loyalty = getAverageVotesWhitParty(independent_list);
  statistics.leastLoyal = getTop(data, "votes_with_party_pct", 10, "<");
  statistics.mostLoyal = getTop(data, "votes_with_party_pct", 10, ">");
  statistics.mostEngaged = getTop(data, "missed_votes", 10, "<");
  statistics.leastEngaged = getTop(data, "missed_votes", 10, ">");
  statistics.totalAvgVotes = ((getAverageVotesWhitParty(democrats_list) + getAverageVotesWhitParty(republicans_list) + getAverageVotesWhitParty(independent_list)) / party_with_members).toFixed(2);
}

function getAverageVotesWhitParty(party) {

  let votesAv = 0;
  for (let i = 0; i < party.length; i++) {
    votesAv += Number(party[i].votes_with_party_pct);
  }
  votesAv /= party.length;
  if (party.length == 0) {
    return null
  }
  return parseFloat(votesAv.toFixed(2))
}

function getTop(data, prop, percentage, order) {

  let sortData;
  if (order == "<") {
    sortData = data.results[0].members.sort(function (a, b) {
      return a[prop] - b[prop];
    })
  } else {
    sortData = data.results[0].members.sort(function (a, b) {
      return b[prop] - a[prop];
    })
  }
  let result = [];
  let i = 0;
  while (i < sortData.length * (percentage / 100) || sortData[i][prop] == result[i - 1][prop]) {
    result.push(sortData[i]);
    i++;
  }
  return result;
}

function myCallback() {
  democrats_list = getPartyMembers(data).democrats;
  republicans_list = getPartyMembers(data).republicans;
  independent_list = getPartyMembers(data).independents;
  updateStatistics();
  console.log(statistics);
}


var democrats_list;
var republicans_list;
var independent_list;
var data;
var congress = 113;//default
var chamber = selected_chamber;//defined in html page
var api_url = 'https://api.propublica.org/congress/v1/' + congress + '/' + chamber + '/members.json';//url request

get_new_data(api_url);